**report 29.12.**
* Název práce: Swap teams/flags
* Popis práce: Natrenovat CycleGAN aby uměl změnit etnicitu člověka na obrázku
* Co z toho leze v soucasnem stavu: fotka, video člověka se změněnou etnicitou
* [odkaz na medium article](https://kouckma2.medium.com/using-cyclegan-to-change-the-ethnicity-of-a-person-36e929140864)

**struktura**
* code from google colab
    * jupyter notebooks from google colab - the most code heavy part
* current frames
    * frames used for the creation of the resulting video
* fancy transformed
    * frames used for the creation of the fancy resulting video
* old
    * some files not important enough to deserve their own folder (for example pdfs of image translations for report)
* utk faces
    * full utk faces dataset
* videa
    * videos used for the creation of the resulting video


